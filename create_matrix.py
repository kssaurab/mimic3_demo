import pandas as pd
import numpy as np

files_dir = "~/projects/physionet.org/works/MIMICIIIClinicalDatabaseDemo/files/version_1_4/"

items       = pd.read_csv(files_dir+"D_ITEMS.csv")

admissions  = pd.read_csv(files_dir+"ADMISSIONS.csv")

chartevents = pd.read_csv(files_dir+"CHARTEVENTS.csv")

deadpatients_subject_ids = admissions[admissions.DEATHTIME.notnull()][['SUBJECT_ID', 'DEATHTIME']]\
                             .rename(columns={'DEATHTIME':'DEATH_TIME'})

subject_ids_with_index = deadpatients_subject_ids.assign(SUBJECT_ID_INDEX = range(0, len(deadpatients_subject_ids)))

admissions_deadpatients = pd.merge(admissions.loc[admissions['SUBJECT_ID'].isin(deadpatients_subject_ids.SUBJECT_ID)],
                                   subject_ids_with_index, on='SUBJECT_ID')

admissions_with_chartevents  = admissions_deadpatients.query('HAS_CHARTEVENTS_DATA==1')\
                                 [['SUBJECT_ID', 'SUBJECT_ID_INDEX', 'HADM_ID', 'DEATH_TIME']]

numeric_items = items.query('PARAM_TYPE=="Numeric"')[['ITEMID']]

numeric_items['ITEM_INDEX'] = range(0, len(numeric_items))

chartevents_filtered = pd.merge(chartevents, numeric_items, on = 'ITEMID')

chartevents_filtered_numeric = chartevents_filtered[chartevents_filtered.ITEM_INDEX.notnull()]

chartevents_merged_with_admissions = pd.merge(chartevents_filtered_numeric,
                                       admissions_with_chartevents, on='SUBJECT_ID')\
                                        [['SUBJECT_ID', 'SUBJECT_ID_INDEX',
                                          'ITEMID', 'ITEM_INDEX', 'CHARTTIME',
                                          'DEATH_TIME', 'VALUENUM']]

final_chartevents = chartevents_merged_with_admissions.assign(
                        CHARTTIME = chartevents_merged_with_admissions['CHARTTIME']\
                            .apply(lambda x:pd.to_datetime(x)),
                        DEATH_TIME = chartevents_merged_with_admissions['DEATH_TIME']\
                            .apply(lambda x:pd.to_datetime(x)))

timesteps = 10
num_subjects = len(deadpatients_subject_ids)
num_items = len(numeric_items)

matrix = np.zeros(timesteps*num_subjects*num_items)

for index, row in final_chartevents.iterrows():
    try:
        matrix[row['SUBJECT_ID_INDEX']*timesteps*num_items + \
               int(np.log10((row['DEATH_TIME'] - row['CHARTTIME']).total_seconds()))*num_items +\
               row['ITEM_INDEX']] = row['VALUENUM']
    except:
        print((row['DEATH_TIME'], row['CHARTTIME']))

matrix_3d = matrix.reshape(num_subjects, timesteps, num_items)
print(matrix_3d)

# print(admissions_with_chartevents.head(50))

